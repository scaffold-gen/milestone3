package main

import (
	_ "embed"
	"errors"
	"flag"
	"fmt"
	"html/template"
	"io"
	"os"
	"path/filepath"
)

type projectConfig struct {
	Name         string
	localPath    string
	RepoURL      string
	StaticAssets bool
}

func setupParseFlags(w io.Writer, args []string) (projectConfig, error) {
	conf := projectConfig{}
	// This is how a local FlagSet object is setup instead of using the global
	// flagset which is difficult to write unit tests for.
	fs := flag.NewFlagSet("scaffold-gen", flag.ContinueOnError)
	// This function sets the output of the flagset to the io.Writer object
	// that is passed in as a parameter to the function
	fs.SetOutput(w)
	fs.StringVar(&conf.Name, "n", "", "Project name")
	fs.StringVar(&conf.localPath, "d", "", "Project location on disk")
	fs.StringVar(&conf.RepoURL, "r", "", "Project remote repository URL")
	fs.BoolVar(&conf.StaticAssets, "s", false, "Project will have static assets or not")
	err := fs.Parse(args)
	if err != nil {
		return conf, err
	}
	if fs.NArg() != 0 {
		return conf, errors.New("No positional parameters expected")
	}
	return conf, err
}

// generateScaffold for now will only just display a message and will be used
// as the entrypoint function for creating the scaffold structure in Milestones
// 3 and 4
func generateScaffold(w io.Writer, conf projectConfig) error {
	fmt.Fprintf(w, "Generating scaffold for project %s in %s\n", conf.Name, conf.localPath)

	if err := createScaffoldDirs(conf); err != nil {
		return err
	}

	if err := createScaffoldFiles(conf); err != nil {
		return err
	}

	return nil
}

func validateConf(conf projectConfig) []error {
	var validationErrors []error
	if len(conf.Name) == 0 {
		validationErrors = append(validationErrors, errors.New("Project name cannot be empty"))
	}
	if len(conf.localPath) == 0 {
		validationErrors = append(validationErrors, errors.New("Project path cannot be empty"))
	}
	if len(conf.RepoURL) == 0 {
		validationErrors = append(validationErrors, errors.New("Project repository URL cannot be empty"))
	}
	return validationErrors
}

func createFile(file string, data []byte) error {
	f, err := os.Create(file)
	if err != nil {
		return err
	}
	defer f.Close()

	if _, err := f.Write(data); err != nil {
		return err
	}

	return nil
}

func createTemplatedFile(file string, tpl string, c projectConfig) error {
	tmpl := template.New("scaffold")
	t, err := tmpl.Parse(tpl)
	if err != nil {
		return err
	}

	f, err := os.Create(file)
	if err != nil {
		return err
	}
	defer f.Close()

	return t.Execute(f, c)
}

//go:embed webapp-templated/static/js/index.js
var staticJS []byte

//go:embed webapp-templated/static/css/styles.css
var staticCSS []byte

//go:embed webapp-templated/handlers/healthcheck.go
var healthCheckGo []byte

//go:embed webapp-templated/server.go.tmpl
var serverTmpl string

//go:embed webapp-templated/go.mod.tmpl
var modTmpl string

//go:embed webapp-templated/handlers/api.go.tmpl
var handlerApiTmpl string

//go:embed webapp-templated/handlers/index.go.tmpl
var handlerIndexTmpl string

//go:embed webapp-templated/handlers/setup.go.tmpl
var handlerSetupTmpl string

func createScaffoldFiles(c projectConfig) error {
	if err := createFile(filepath.Join(c.localPath, "handlers", "healthcheck.go"), healthCheckGo); err != nil {
		return err
	}

	if err := createTemplatedFile(filepath.Join(c.localPath, "handlers", "api.go"), handlerApiTmpl, c); err != nil {
		return err
	}

	if err := createTemplatedFile(filepath.Join(c.localPath, "handlers", "setup.go"), handlerSetupTmpl, c); err != nil {
		return err
	}

	if err := createTemplatedFile(filepath.Join(c.localPath, "server.go"), serverTmpl, c); err != nil {
		return err
	}

	if err := createTemplatedFile(filepath.Join(c.localPath, "go.mod"), modTmpl, c); err != nil {
		return err
	}

	if c.StaticAssets {
		if err := createFile(filepath.Join(c.localPath, "static", "js", "index.js"), staticJS); err != nil {
			return err
		}

		if err := createFile(filepath.Join(c.localPath, "static", "css", "styles.css"), staticCSS); err != nil {
			return err
		}

		if err := createTemplatedFile(filepath.Join(c.localPath, "handlers", "index.go"), handlerIndexTmpl, c); err != nil {
			return err
		}
	}

	return nil

}

func createScaffoldDirs(conf projectConfig) error {
	if _, err := os.Stat(conf.localPath); !os.IsNotExist(err) {
		return fmt.Errorf("local path already exists")
	}

	if err := os.MkdirAll(filepath.Join(conf.localPath, "handlers"), 0755); err != nil {
		return err
	}

	if conf.StaticAssets {
		if err := os.MkdirAll(filepath.Join(conf.localPath, "static", "js"), 0755); err != nil {
			return err
		}
		if err := os.MkdirAll(filepath.Join(conf.localPath, "static", "css"), 0755); err != nil {
			return err
		}
	}

	return nil
}

func main() {
	conf, err := setupParseFlags(os.Stdout, os.Args[1:])
	if err != nil {
		fmt.Println(err)
		os.Exit(1)
	}

	errors := validateConf(conf)
	if len(errors) != 0 {
		for _, e := range errors {
			fmt.Println(e)
		}
		os.Exit(1)
	}

	if err := generateScaffold(os.Stdout, conf); err != nil {
		fmt.Println(err)
		os.Exit(1)
	}
}
